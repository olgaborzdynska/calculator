//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Calculator.rc
//
#define IDD_CALCULATOR_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON1                     1000
#define IDC_BUTTON2                     1001
#define IDC_BUTTON3                     1002
#define IDC_BUTTON4                     1003
#define IDC_BUTTON5                     1004
#define IDC_BUTTON6                     1005
#define IDC_BUTTON7                     1006
#define IDC_BUTTON8                     1007
#define IDC_BUTTON9                     1008
#define IDC_BUTTON_COMA                 1009
#define IDC_BUTTON0                     1010
#define IDC_BUTTON_ANS                  1011
#define IDC_BUTTON_DIV                  1012
#define IDC_BUTTON_MULT                 1013
#define IDC_BUTTON_MINUS                1014
#define IDC_BUTTON_PLUS                 1015
#define IDC_BUTTON_CE                   1016
#define IDC_BUTTON_C                    1017
#define IDC_BUTTON_SQRT                 1018
#define IDC_BUTTON_EQUAL                1019
#define IDC_EDIT1                       1020
#define IDC_EDIT_HIST                   1022
#define IDC_EDIT5                       1024
#define IDC_BUTTON_SAVE                 1025

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1026
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
