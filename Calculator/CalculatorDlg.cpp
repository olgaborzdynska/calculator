
// CalculatorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Calculator.h"
#include "CalculatorDlg.h"
#include "afxdialogex.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CCalculatorDlg dialog



CCalculatorDlg::CCalculatorDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_CALCULATOR_DIALOG, pParent)
	, historyText(_T(""))
	, output(_T(""))
	, previous(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCalculatorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_HIST, historyText);
	DDX_Text(pDX, IDC_EDIT1, output);
	DDX_Text(pDX, IDC_EDIT5, previous);
}

BEGIN_MESSAGE_MAP(CCalculatorDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CCalculatorDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CCalculatorDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CCalculatorDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CCalculatorDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CCalculatorDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &CCalculatorDlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &CCalculatorDlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON8, &CCalculatorDlg::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON9, &CCalculatorDlg::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON0, &CCalculatorDlg::OnBnClickedButton0)
	ON_BN_CLICKED(IDC_BUTTON_PLUS, &CCalculatorDlg::OnBnClickedButtonPlus)
	ON_BN_CLICKED(IDC_BUTTON_MINUS, &CCalculatorDlg::OnBnClickedButtonMinus)
	ON_BN_CLICKED(IDC_BUTTON_MULT, &CCalculatorDlg::OnBnClickedButtonMult)
	ON_BN_CLICKED(IDC_BUTTON_DIV, &CCalculatorDlg::OnBnClickedButtonDiv)
	ON_BN_CLICKED(IDC_BUTTON_EQUAL, &CCalculatorDlg::OnBnClickedButtonEqual)
	ON_BN_CLICKED(IDC_BUTTON_ANS, &CCalculatorDlg::OnBnClickedButtonAns)
	ON_BN_CLICKED(IDC_BUTTON_SQRT, &CCalculatorDlg::OnBnClickedButtonSqrt)
	ON_BN_CLICKED(IDC_BUTTON_COMA, &CCalculatorDlg::OnBnClickedButtonComa)
	ON_BN_CLICKED(IDC_BUTTON_CE, &CCalculatorDlg::OnBnClickedButtonCe)
	ON_BN_CLICKED(IDC_BUTTON_C, &CCalculatorDlg::OnBnClickedButtonC)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CCalculatorDlg::OnBnClickedButtonSave)
END_MESSAGE_MAP()


// CCalculatorDlg message handlers

BOOL CCalculatorDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	//wyswietla 0 w edit control
	GetDlgItem(IDC_EDIT1)->SetWindowText(_T("0"));
	
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	ShowWindow(SW_MINIMIZE);

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}


// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCalculatorDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCalculatorDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

double firstNumber;
double secondNumber;
double result;
char operation;

void CCalculatorDlg::addDigit(char digit)
{
	if (output == "0") {
		output = digit;
	}
	else {
		output += digit;
	}
	UpdateData(FALSE);
}

void CCalculatorDlg::onComma()
{
	if (output.Find('.') == -1)
	{
		output += '.';
	}
	UpdateData(FALSE);
}

void CCalculatorDlg::onPlus()
{
	operation = '+';
	firstNumber = _wtof(output);
	previous = output + ' ' + operation;
	output = '0';
	UpdateData(FALSE);
}

void CCalculatorDlg::onMinus()
{
	operation = '-';
	firstNumber = _wtof(output);
	previous = output + ' ' + operation;
	output = '0';
	UpdateData(FALSE);
}

void CCalculatorDlg::onMultiply()
{
	operation = '*';
	firstNumber = _wtof(output);
	previous = output + ' ' + operation;
	output = '0';
	UpdateData(FALSE);
}

void CCalculatorDlg::onDivide()
{
	operation = '/';
	firstNumber = _wtof(output);
	previous = output + ' ' + operation;
	output = '0';
	UpdateData(FALSE);
}

void CCalculatorDlg::onEqual()
{
	secondNumber = _wtof(output);
	historyText = historyText + previous + ' ' + output + _T(" = ");
	previous = "";

	switch (operation) {
	case '+':
		result = firstNumber + secondNumber;
		break;
	case '-':
		result = firstNumber - secondNumber;
		break;
	case '*':
		result = firstNumber * secondNumber;
		break;
	case '/':
		result = firstNumber / secondNumber;
		break;
	}
	output.Format(_T("%g"), result);
	historyText += output;
	historyText += "\r\n";
	UpdateData(FALSE);
	output = "0";
}

void CCalculatorDlg::OnBnClickedButton1()
{
	addDigit('1');
}

void CCalculatorDlg::OnBnClickedButton2()
{
	addDigit('2');
}

void CCalculatorDlg::OnBnClickedButton3()
{
	addDigit('3');
}

void CCalculatorDlg::OnBnClickedButton4()
{
	addDigit('4');
}

void CCalculatorDlg::OnBnClickedButton5()
{
	addDigit('5');
}

void CCalculatorDlg::OnBnClickedButton6()
{
	addDigit('6');
}

void CCalculatorDlg::OnBnClickedButton7()
{
	addDigit('7');
}

void CCalculatorDlg::OnBnClickedButton8()
{
	addDigit('8');
}

void CCalculatorDlg::OnBnClickedButton9()
{
	addDigit('9');
}

void CCalculatorDlg::OnBnClickedButton0()
{
	addDigit('0');
}

void CCalculatorDlg::OnBnClickedButtonComa()
{
	onComma();
}

void CCalculatorDlg::OnBnClickedButtonPlus()
{
	onPlus();
}

void CCalculatorDlg::OnBnClickedButtonMinus()
{
	onMinus();
}

void CCalculatorDlg::OnBnClickedButtonMult()
{
	onMultiply();
}

void CCalculatorDlg::OnBnClickedButtonDiv()
{
	onDivide();
}

void CCalculatorDlg::OnBnClickedButtonSqrt()
{
	firstNumber = _wtof(output);
	historyText += _T("sqrt(") + output +_T(") = ");
	result = sqrt(firstNumber);
	output.Format(_T("%g"), result);
	historyText += output;
	historyText += "\r\n";
	operation = '0';
	UpdateData(FALSE);
	output = "0";
}

void CCalculatorDlg::OnBnClickedButtonEqual()
{
	onEqual();
}

void CCalculatorDlg::OnBnClickedButtonAns()
{
	output.Format(_T("%g"), result);
	UpdateData(FALSE);

}

void CCalculatorDlg::OnBnClickedButtonCe()
{
	output = "0";
	UpdateData(FALSE);
}

void CCalculatorDlg::OnBnClickedButtonC()
{
	output = "0";
	firstNumber = 0;
	previous = "";
	UpdateData(FALSE);
}



//keyboard support
BOOL CCalculatorDlg::PreTranslateMessage(MSG * pMsg)
{
	int x = (int)pMsg->wParam;

	if (pMsg->message == WM_KEYDOWN)
	{
		if (x == 48 || x == 96) { addDigit('0'); }
		if (x == 49 || x == 97) { addDigit('1'); }
		if (x == 50 || x == 98) { addDigit('2'); }
		if (x == 51 || x == 99) { addDigit('3'); }
		if (x == 52 || x == 100) { addDigit('4'); }
		if (x == 53 || x == 101) { addDigit('5'); }
		if (x == 54 || x == 102) { addDigit('6'); }
		if (x == 55 || x == 103) { addDigit('7'); }
		if (x == 56 || x == 104) { addDigit('8'); }
		if (x == 57 || x == 105) { addDigit('9'); }
	}

		if (x == 42) { onMultiply(); }
		if (x == 43) { onPlus(); }
		if (x == 45) { onMinus(); }
		if (x == 47) { onDivide(); }
		if (x == 44 || x == 46) { onComma(); }
		if (x == 61) { onEqual(); }

	return CDialog::PreTranslateMessage(pMsg);
}

void CCalculatorDlg::OnBnClickedButtonSave()
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	CStdioFile file;
	if (historyText.GetLength() == 0) {

		AfxMessageBox(L"There is no history to be saved.");
		return;
	}
	file.Open(L"./calcHistory.txt", CFile::modeCreate |
		CFile::modeWrite | CFile::typeText);
	file.WriteString(historyText);
	file.Close();
	AfxMessageBox(L"History saved.");
}
